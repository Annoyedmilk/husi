//
// Created by Marco Müller on 16.03.23.
//

#include <stdio.h>

int main() {
    char operator;
    double zahl1, zahl2, resultat;

    printf("Gib deine erste Zahl ein: ");
    scanf("%lf", &zahl1);

    printf("Gib deine zweite Zahl ein: ");
    scanf("%lf", &zahl2);

    printf("Wähle einen Operanden (+, -, *, /): ");
    scanf(" %c", &operator);

    switch (operator) {
        case '+':
            resultat = zahl1 + zahl2;
            break;

        case '-':
            resultat = zahl1 - zahl2;
            break;

        case '*':
            resultat = zahl1 * zahl2;
            break;

        case '/':
            resultat = zahl1 / zahl2;
            break;

        default:
            printf("Invalider Operand.");
            return 1;
    }

    printf("Resultat: %.2lf", resultat);
    return 0;
}
